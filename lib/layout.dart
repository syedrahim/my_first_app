import 'package:flutter/material.dart';

class layout extends StatelessWidget {
  var borderDecoration = BoxDecoration(border: Border.all(color: Colors.black));
  List containerNames = ['Name','Description','Price'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Layout'),
      ),
      body: Center(
        child: Container(
          width: 400,
          height: 200,
          decoration: borderDecoration,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  decoration: borderDecoration,
                  child: Center(child: Text('Image')),
                ),
                Column(
                  children: [
                    for (var i = 0; i < 3; i++)
                      Padding(
                        padding: const EdgeInsets.all(9.0),
                        child: Container(
                          width: 200,
                          height: 35,
                          decoration: borderDecoration,
                          child: Center(child: Text(containerNames[i])),
                        ),
                      )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
