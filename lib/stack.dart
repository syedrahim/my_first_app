import 'package:flutter/material.dart';

class MyStackCard extends StatefulWidget {
  const MyStackCard({Key? key}) : super(key: key);

  @override
  State<MyStackCard> createState() => _MyStackCardState();
}

class _MyStackCardState extends State<MyStackCard> {
  var textStyle = const TextStyle(fontSize: 15);
  var buttonStyle = const TextStyle(fontSize: 15, color: Colors.blue);
  var overlayTextStyle = const TextStyle(
      fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold);
  var boxShadow = const BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
    boxShadow: [
      BoxShadow(
        color: Color(0xffDDDDDD),
        blurRadius: 6.0,
        spreadRadius: 2.0,
        offset: Offset(0.0, 0.0),
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 400,
        height: 400,
        decoration: boxShadow,
        // color: Colors.grey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10)),
                child: Image.asset(
                  'Assets/img.png',
                  fit: BoxFit.fill,
                ),
              ),
              Positioned(
                left: 10,
                bottom: 20,
                child: Text(
                  'Top 10 Australian Beaches',
                  style: overlayTextStyle,
                ),
              )
            ]),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Number 10', style: textStyle),
                  Text('Whitehaven Beach', style: textStyle),
                  Text('Whitsunday Island, Whitsunday islands',
                      style: textStyle),
                  Container(
                    padding: const EdgeInsets.fromLTRB(0.0, 20, 0, 0),
                    width: 150,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'SHARE',
                          style: buttonStyle,
                        ),
                        Text('EXPLORE', style: buttonStyle),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
