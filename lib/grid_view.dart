import 'package:flutter/material.dart';

class MyGridView extends StatefulWidget {
  const MyGridView({Key? key}) : super(key: key);

  @override
  State<MyGridView> createState() => _MyGridViewState();
}

class _MyGridViewState extends State<MyGridView> {
  var array = [
    "He'd have you all unravel at the",
    "Heed not the rabble",
    "Sound of screams but the",
    "Who scream",
    "Revolution is coming",
    "Revolution, they..."
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Grid View'),
        ),
        body: GridView.builder(
            padding: EdgeInsets.all(20.0),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, crossAxisSpacing: 20, mainAxisSpacing: 20),
            itemCount: array.length,
            itemBuilder: (context, index) {
              return Container(
                color: Colors.teal[(index + 1) * 100],
                child: Text(
                  array[index],
                  style: TextStyle(fontSize: 20),
                ),
                padding: EdgeInsets.all(20.0),
              );
            })

        // Column(
        //   children: [
        //     Expanded(
        //       child: GridView.count(
        //         crossAxisCount: 2,
        //         crossAxisSpacing: 10,
        //         mainAxisSpacing: 10,
        //         children: [
        //           for (var i = 100; i < 1000; i += 100)
        //             Container(
        //               color: Colors.green[i],
        //             )
        //         ],
        //       ),
        //     ),
        //     Container(height: 100,),
        //     Expanded(
        //         child: GridView.extent(
        //       maxCrossAxisExtent: 150,
        //       crossAxisSpacing: 10,
        //       mainAxisSpacing: 10,
        //       children: [
        //         for (var i = 100; i < 1000; i += 100)
        //           Container(
        //             color: Colors.pink[i],
        //           )
        //       ],
        //     ))
        //   ],
        // ),
        );
  }
}
