import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget TextFieldBuilder(
    {displayText,
    validator,
    onSaved,
    keyBoardType,
    controller,
    isOnlyDigits = false,
    obscureText = false,
    isOnlyDate = false}) {
  return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      child: TextFormField(
        decoration: InputDecoration(
            labelText: displayText,
            filled: true,
            fillColor: Colors.grey[300],
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black, width: 0.0)),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0)),
            floatingLabelStyle: TextStyle(color: Colors.black),
            errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: .5))),
        onSaved: onSaved,
        validator: validator,
        keyboardType: keyBoardType,
        controller: controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        style: TextStyle(fontSize: 20, color: Colors.black),
        textCapitalization: TextCapitalization.characters,
        inputFormatters: [
          if (isOnlyDigits) FilteringTextInputFormatter.digitsOnly,
          // if (isOnlyDate) FilteringTextInputFormatter.allow(RegExp(r'/[0-9]/')),
        ],
        obscureText: obscureText,
      ));
}

CheckboxListTile renderCheckbox({value, onChanged, title = ""}) {
  return CheckboxListTile(
    activeColor: Colors.black,
    controlAffinity: ListTileControlAffinity.leading,
    title: Text(title),
    value: value,
    onChanged: onChanged,
    contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 10),
  );
}

class FormPage extends StatefulWidget {
  const FormPage({Key? key}) : super(key: key);

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final formKey = GlobalKey<FormState>();

  String name = "";
  String email = "";
  int? phone;
  String password = "";
  String confirmPassword = "";
  String? gender;
  String? dateOfBirth;
  bool isChecked = false;

  final List<String> list = <String>['One', 'Two', 'Three', 'Four'];
  String dropdownValue = 'One';

  final TextEditingController _pass = TextEditingController();

  var nameValidator = (value) {
    if (value.isEmpty || !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
      return "Enter Valid Name";
    }
    return null;
  };
  var emailValidator = (value) {
    if (value.isEmpty ||
        !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)) {
      return "Enter Valid Email Address";
    }
    return null;
  };
  var dateValidator = (value) {
    if (value.isEmpty ||
        !RegExp(r"^(0?[1-9]|1[0-2])[\/](0?[1-9]|[12]\d|3[01])[\/](19|20)\d{2}$")
            .hasMatch(value) ) {
      return "Enter Valid Date";
    }
    return null;
  };
  var phoneValidator = (value) {
    if (value.isEmpty ||
        !RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
            .hasMatch(value)) {
      return "Enter Correct Phone Number";
    }
    return null;
  };
  var passwordValidator = (value) {
    if (value.isEmpty ||
        !RegExp(r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            .hasMatch(value)) {
      return "at least 8 characters,\n must contain at least 1 uppercase letter, 1 lowercase letter,\n and 1 number, Can contain special characters";
    }
    return null;
  };
  var genderValidator = (value) {
    if (value.isEmpty || (value!='male' && value!='female' && value!='other')) {
      return "enter valid gender";
    }
    return null;
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Forms Validation'),
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: ListView(
            children: [
              TextFieldBuilder(
                  displayText: "Email",
                  validator: emailValidator,
                  onSaved: (value) => setState(() => email = value!),
                  keyBoardType: TextInputType.emailAddress),
              TextFieldBuilder(
                  displayText: "Username",
                  validator: nameValidator,
                  onSaved: (value) => setState(() => name = value!),
                  keyBoardType: TextInputType.text),
              TextFieldBuilder(
                  displayText: "Date of Birth (mm/dd/yyyy)",
                  validator: dateValidator,
                  onSaved: (value) => setState(() => dateOfBirth = value!),
                  keyBoardType: TextInputType.datetime,
                  // isOnlyDate: true
              ),
              // InputDatePickerFormField( firstDate: DateTime.utc(1900, 1, 1), lastDate: DateTime.utc(2099, 1, 1)),
              TextFieldBuilder(
                  displayText: "Gender (male/female/other)",
                  validator: genderValidator,
                  onSaved: (value) => setState(() => gender = value!),
                  keyBoardType: TextInputType.text),
              TextFieldBuilder(
                  displayText: "Phone",
                  validator: phoneValidator,
                  onSaved: (value) =>
                      setState(() => phone = int.tryParse(value) ?? 0),
                  keyBoardType: TextInputType.phone,
                  isOnlyDigits: true),
              TextFieldBuilder(
                  displayText: "Password (atleast 8 characters)",
                  validator: passwordValidator,
                  onSaved: (value) => setState(() => password = value!),
                  keyBoardType: TextInputType.text,
                  controller: _pass,
                  obscureText: true),
              TextFieldBuilder(
                  displayText: "Confirm Password",
                  validator: (value) {
                    if (value.isEmpty) return 'Enter valid password';
                    if (value != _pass.text) return 'Not matching';
                    return null;
                  },
                  obscureText: true,
                  onSaved: (value) => setState(() => confirmPassword = value!),
                  keyBoardType: TextInputType.text),
              renderCheckbox(
                  value: isChecked,
                  onChanged: (isChecked) =>
                      setState(() => this.isChecked = isChecked!),
                  title:
                      "Yes, I agree to EXLR's Privacy Policy, Terms and Services and Billing Terms"),
              ElevatedButton(
                onPressed: () {
                  final isValid = formKey.currentState!.validate();
                  if (!isChecked) {
                    final snackBar = SnackBar(
                        content: Text(
                            'Check the box to accept the terms and conditions!..'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                  if (isValid & isChecked) {
                    formKey.currentState!.save();
                    final message =
                        'Sucess $name \t $email \t $phone \t $password';
                    final snackBar = SnackBar(content: Text(message));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                style: TextButton.styleFrom(
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  fixedSize: Size.fromHeight(50),
                ),
                child: const Text(
                  'Submit',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
