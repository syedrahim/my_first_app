import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class staggeredView extends StatefulWidget {
  const staggeredView({Key? key}) : super(key: key);

  @override
  State<staggeredView> createState() => _staggeredViewState();
}

class _staggeredViewState extends State<staggeredView> {
  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.all(8), child: masonaryLayout(context));
  }
}

Widget masonaryLayout(BuildContext context) {
  return MasonryGridView.builder(
    scrollDirection: Axis.vertical,
    crossAxisSpacing: 8,
    mainAxisSpacing: 8,
    gridDelegate: SliverSimpleGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
    ),
    itemCount: 10,
    itemBuilder: (context, index) {
      return Container(
          color: Colors.white,
          child: Column(
            children: [
              Image.network(
                'https://source.unsplash.com/random?sig=$index',
                fit: BoxFit.contain,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:EdgeInsets.fromLTRB(0,0,0,15),
                      child: Text(
                        'Mysterios of universe',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    Text('20 Jan 2013'),
                    Text('Carl Sagan'),
                  ],
                ),
              )
            ],
          ));
    },
  );
}
