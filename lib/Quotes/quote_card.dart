import 'package:flutter/material.dart';
import 'Quote.dart';

class QuoteCard extends StatelessWidget {
  final Quote quote;
  final Function delete;
  QuoteCard({required this.quote, required this.delete});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              quote!.text,
              style: TextStyle(
                fontFamily: 'VarelaRound',
                fontSize: 25.0,
              ),
            ),
            SizedBox(
              height: 6.0,
            ),
            Text(quote!.author,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
            SizedBox(
              height: 8.0,
            ),
            TextButton.icon(
              onPressed: () => delete(),
              label: Text('Delete Quote'),
              icon: Icon(Icons.delete),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
