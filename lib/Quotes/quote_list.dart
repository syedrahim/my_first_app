import 'package:flutter/material.dart';
import 'Quote.dart';
import 'quote_card.dart';

class QuoteList extends StatefulWidget {
  const QuoteList({Key? key}) : super(key: key);

  @override
  State<QuoteList> createState() => _QuoteListState();
}

class _QuoteListState extends State<QuoteList> {
  List<Quote> quotes = [
    Quote(
        text:
        "Keep smiling, because life is a beautiful thing and there's so much to smile about.",
        author: "john"),
    Quote(text: "Life is a long lesson in humility.", author: "Smith"),
    Quote(text: "Live and Let live.", author: "john"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown,

      appBar: AppBar(
        title: Text('Lists'),
        centerTitle: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
            children: quotes.map((quote) => QuoteCard(quote:quote,delete: () {
              setState(() {
                quotes.remove(quote);
              });
            })).toList()),
      ),
    );
  }
}
