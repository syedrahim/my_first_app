import 'package:flutter/material.dart';


class MyListView extends StatelessWidget {
  const MyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List View'),
      ),
      body: _buildListView(),
    );
  }

  Widget _buildListView() {
    return Container(
      height: 400,
      child: ListView.separated(
        padding: EdgeInsets.all(10),
          shrinkWrap: true,
          itemCount: 10, separatorBuilder: (context,index)=>Divider(),
          itemBuilder: (context,index) {
            return Container(
              color: Colors.yellow[(index+1)*100],
              child: ListTile(
                title: Text('This is List number #$index'),
                leading: Icon(Icons.thumb_up_alt),
                subtitle: Text('This is subtitle'),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
            );

          }
      ),
    );
  }
}
    //   ListView.builder(itemCount:15,itemBuilder: (context,index) {
    //   return Container(
    //     child: ListTile(
    //       onTap: () {
    //         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //           content: Text('#$index pressed!'),
    //           duration: Duration(milliseconds: 300),
    //         ));
    //       },
    //       title: Text('This is List number #$index'),
    //       leading: Icon(Icons.thumb_up_alt),
    //       subtitle: Text('This is subtitle'),
    //       trailing: Icon(Icons.arrow_forward_ios),
    //       textColor: Colors.white,
    //       iconColor: Colors.white,
    //     ),
    //     color: Colors.blue,
    //     margin: EdgeInsets.all(10),
    //   );
    // });

