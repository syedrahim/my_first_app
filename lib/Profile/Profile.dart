import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  int count=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Profile',
              style: TextStyle(
                  fontFamily: 'IndieFlower',
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0,
                  color: Colors.amber),
            ),
          ],
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.grey.shade700,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.plus_one,color: Colors.black,),
        backgroundColor: Colors.amber,
        splashColor: Colors.red,
        hoverColor: Colors.red,
        onPressed: () {
          setState(() {
            count+=1;
          });
        },
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
                child: CircleAvatar(
              backgroundImage: AssetImage('Assets/image1.jpg'),
              radius: 50.0,
            )),
            Divider(
              height: 80.0,
              color: Colors.amber,
            ),
            Text(
              'Name',
              style: TextStyle(
                  fontFamily: 'IndieFlower',
                  color: Colors.amber,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'Syed Mohammed Rahimuddin',
              style: TextStyle(
                  color: Colors.grey.shade300,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0),
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              'Flutter Level',
              style: TextStyle(
                  fontFamily: 'IndieFlower',
                  color: Colors.amber,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              '$count',
              style: TextStyle(
                  color: Colors.grey.shade300,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0),
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              children: [
                Icon(
                  Icons.email,
                  color: Colors.amber,
                  size: 40.0,
                ),
                SizedBox(
                  width: 20.0,
                ),
                Flexible(
                  child: Text(
                    'syed.rahimuddin@rootquotient.com',
                    style: TextStyle(
                        color: Colors.grey.shade300,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
