import 'package:flutter/material.dart';

class MyTableWidget extends StatefulWidget {
  const MyTableWidget({Key? key}) : super(key: key);

  @override
  State<MyTableWidget> createState() => _MyTableWidgetState();
}

Widget tableCell(text, textStyle) {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Text(
      text,
      style: textStyle,
    ),
  );
}

TableRow _tableRow(person) {
  TextStyle textStyle = TextStyle(
    fontSize: 20,
    fontWeight: person['isHeader'] ? FontWeight.bold : FontWeight.normal,
    fontStyle: person['isHeader'] ? FontStyle.italic : FontStyle.normal,
  );

  return TableRow(children: [
    tableCell(person['name'], textStyle),
    tableCell(person['age'], textStyle),
    tableCell(person['Role'], textStyle),
  ]);
}

class _MyTableWidgetState extends State<MyTableWidget> {
  var array = [
    {'name': 'Name', 'age': 'Age', 'Role': 'Role', 'isHeader': true},
    {'name': 'Sarah', 'age': '19', 'Role': 'Student', 'isHeader': false},
    {'name': 'Janine', 'age': '43', 'Role': 'Professor', 'isHeader': false},
    {
      'name': 'William',
      'age': '27',
      'Role': 'Associate Professor',
      'isHeader': false
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Table(
        border: const TableBorder(
            horizontalInside: BorderSide(
              width: .5,
              color: Colors.grey,
            )),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: const {
          0: FixedColumnWidth(100.0),
          1: FixedColumnWidth(70.0),
          2: FixedColumnWidth(230.0),
        },
        children: [
          for (var i = 0; i < array.length; i++) _tableRow(array[i]),
        ],
      ),
    );
  }
}
