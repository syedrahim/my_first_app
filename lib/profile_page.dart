import 'package:flutter/material.dart';
import 'my_flutter_app_icons.dart';
import 'stack.dart';
import 'table.dart';
import 'wrap.dart';
import 'staggeredGridView.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _currentIndex = 0;

  List tabs = [
    MyTableWidget(),
    wrap(),
    MyStackCard(),
    staggeredView(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // toolbarHeight: 100,
        // title: Text('Profile Page'),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu_sharp,
            size: 40,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              MyFlutterApp.timeline,
              size: 40,
            ),
            padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
          ),
        ],
        elevation: 0.0,
        backgroundColor: Colors.pink,
      ),
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon:Icon(Icons.table_bar),label: 'Table'),
          BottomNavigationBarItem(icon:Icon(Icons.next_plan_sharp,color: Colors.black,),label: 'Wrap',),
          BottomNavigationBarItem(icon:Icon(Icons.indeterminate_check_box),label: 'Stack'),
          BottomNavigationBarItem(icon:Icon(Icons.grid_3x3),label: 'Grid'),
          BottomNavigationBarItem(icon:Icon(Icons.person),label: 'Profile'),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex=index;
          });
        },
      ),
    );
  }
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.pink, Colors.red])
          ),
          height: 220,
          width: 500,
          // color: Colors.redAccent,
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage('Assets/image1.jpg'),
                radius: 60,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Samuel Thibault',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 40,
                    fontFamily: 'PoiretOne'),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'INTRALINKS SYSTEMS',
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontFamily: 'PoiretOne',
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        Container(
          height: 150,
          width: 500,
          color: Colors.red[100],
          padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '35',
                    style: TextStyle(
                        fontSize: 35,
                        color: Colors.red[400],
                        fontFamily: 'PoiretOne',
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Teams',
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.red[400],
                        fontFamily: 'PoiretOne',
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              VerticalDivider(
                  width: .5, color: Colors.red[400], thickness: .5),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '234',
                    style: TextStyle(
                        fontSize: 35,
                        color: Colors.red[400],
                        fontFamily: 'PoiretOne',
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Reports',
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.red[400],
                        fontFamily: 'PoiretOne',
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          height: 350,
          width: 500,
          color: Colors.black87,
          child: ListView.builder(
              itemCount: 3,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Container(
                  height: 250,
                  width: 350,
                  // color: Colors.grey[800],
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [.7,1],
                          colors: [Colors.black, Colors.grey])
                  ),
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 40),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Destination Report',
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontFamily: 'PoiretOne',
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1)),
                            Text(
                              'REGIONAL THREATS',
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.red[500],
                                  fontFamily: 'PoiretOne',
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey[500],
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '2,365',
                                  style: TextStyle(
                                      fontSize: 30,
                                      color: Colors.white,
                                      fontFamily: 'PoiretOne',
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1),
                                ),
                                Text(
                                  'Requests',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                      fontFamily: 'PoiretOne',
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1),
                                )
                              ],
                            ),
                            VerticalDivider(
                                width: .5,
                                color: Colors.red[400],
                                thickness: .5),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '6,908',
                                  style: TextStyle(
                                      fontSize: 30,
                                      color: Colors.white,
                                      fontFamily: 'PoiretOne',
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1),
                                ),
                                Text(
                                  'Latches',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                      fontFamily: 'PoiretOne',
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1),
                                )
                              ],
                            ),
                          ],
                        )
                      ]),
                );
              }),
        )
      ],
    );
  }
}

