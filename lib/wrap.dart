import 'package:flutter/material.dart';

class wrap extends StatelessWidget {
  var buttonStyle = TextButton.styleFrom(
      foregroundColor: Colors.white,
      backgroundColor: Colors.green,
      fixedSize: Size.fromWidth(100));

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
          height: 400,
          width: 400,
          color: Colors.grey[300],
          child: Wrap(
            alignment: WrapAlignment.spaceEvenly,
            runAlignment: WrapAlignment.spaceEvenly,
            direction: Axis.horizontal,
            spacing: 10,
            runSpacing: 10,
            children: [
              for (int i = 0; i < 10; i++)
                TextButton(
                    onPressed: () {},
                    child: Text('Button ${i + 1}'),
                    style: buttonStyle),
            ],
          ),
        ));
  }
}
